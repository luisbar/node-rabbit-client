#### How to run
```sh
npm i && npm start
```

:warning: First you have to install and run your Rabbit Server

#### Rabbit

###### What is [Rabbit](https://www.rabbitmq.com/)?

Its a Message Oriented Middleware (MOM) and it uses the message protocol called Advanced Message Queuing Protocol (AMQP)

###### How to install?

You can install with options shown [here](https://www.rabbitmq.com/download.html)

###### How to run Rabbit Server?
```sh
rabbitmq-server start
```

:warning: Your Rabbit server runs on port 5672 by default and the Rabbit dashboard runs on port 15672
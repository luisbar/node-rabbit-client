const amqp = require('amqplib/callback_api');
const { lineReader } = require('./line-reader.js')

amqp.connect('amqp://localhost', (error0, connection) => {
  if (error0) throw error0;

  connection.createChannel((error1, channel) => {
    if (error1) throw error1;

    const queue = 'task_queue';

    channel.assertQueue(queue, {
      durable: true,/* to persist the queue on disk in order
      to avoid lost of it when Rabbit has a problem */
    });

    sendMessage(channel, queue);
  });
});

const sendMessage = (channel, queue) => {
  lineReader.question('Your message: \n', (msg) => {
    channel.sendToQueue(queue, Buffer.from(msg), {
      persistent: true,/* to persist the message on disk in order to
      avoid lost of it when Rabbit has a problem, If you need a stronger 
      guarantee then you can use https://www.rabbitmq.com/confirms.html */
    });
    setTimeout(() => sendMessage(channel, queue), 500);
  });
}
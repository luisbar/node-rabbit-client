const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', (error0, connection) => {
  if (error0) throw error0;
  connection.createChannel((error1, channel) => {
    if (error1) throw error1;

    const queue = 'task_queue';

    channel.assertQueue(queue, {
      durable: true,/* to persist the queue on disk in order 
      to avoid lost of it when Rabbit has a problem */
    });

    channel.consume(queue, (msg) => {
      const length = msg.content.toString().length;
      setTimeout(() => console.log('Consumer has received next message:', msg.content.toString()), 1000 * length)
    }, {
        noAck: false,/* in order to make sure a message is never lost,
        An ack(nowledgement) is sent back by the consumer to tell RabbitMQ
        that a particular message has been received, processed and that 
        RabbitMQ is free to delete it. */
    });
  });
});

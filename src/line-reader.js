const readline = require('readline');

const lineReader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

module.exports = {
  lineReader,
};
